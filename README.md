# README #

This is a simple program that converts a long phrase, e.g., Portable Network Graphics, to its acronym (PNG).

### Prerequisite ###

* Java 8
* Build tool -
I used Maven to build and run the test cases. The version is Apache Maven 3.5.0 (ff8f5e7444045639af65f6095c62210b5713f426; 2017-04-03T20:39:06+01:00)

### How do I run it? ###

* Git clone - but if you are happy to contribute I can do that too. :)
* Dependencies -
The pom.xml should explain the dependencies required.
The list is:
1. Maven:junit:junit:4.12
2. Maven:org.hamcrest:hamcrest-core:1.3
3. Maven:org.hamcrest:hamcrest-library:1.3

* How to run tests -
You can run it by mvn test or via the Maven Projects from Intellij in my case.
