/**
 * Created by joel huang on 01/06/2017.
 */

package com.mycompany.app;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class Acronym {

    private final String acronym;

    /**
     * Constructs and initialise an Acronym by passing in a string phrase.
     * @param phrase an input string.
     *
     */
    public Acronym(String phrase) {
        acronym = convertAcronym(phrase);
    }

    /**
     * Return an Acronym based on the input string phrase.
     * @param () an input string.
     *
     */
    public String get() {
        return acronym;
    }

    // This is the method that does the heavy lifting.
    private String convertAcronym(String phrase) {
        Pattern pattern;
        Matcher matcher = null;

        // Validate if the regex, e.g., passing in "[}",
        // and print the info to identify the error.
        try {
            // Define the search pattern here.
            pattern = Pattern.compile("[A-Z]+[a-z]*|[a-z]+");
            matcher = pattern.matcher(phrase);
        } catch (PatternSyntaxException pse) {
            System.out.format("There is a problem" +
                    " with the regex!%n");
            System.out.format("The pattern is: %s%n",
                    pse.getPattern());
            System.out.format("The description is: %s%n",
                    pse.getDescription());
            System.out.format("The message is: %s%n",
                    pse.getMessage());
            System.out.format("The index is: %s%n",
                    pse.getIndex());
            System.exit(0);
        }
        final StringBuilder sb = new StringBuilder();
        // Test if match successes.
        while (matcher.find()){
            // For each sub-sequence, get the initial char.
            sb.append(matcher.group().charAt(0));
        }
        // Turn the acronym to upper case.
        return sb.toString().toUpperCase();
    }
}